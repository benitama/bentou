class Project < ActiveRecord::Base
    validates :shouhin, presence: { message: "商品入力してください" }
    validates :tanka, presence: { message: "単価は半角数字で入力してください" }
    validates :zaikosu, presence: { message: "在庫は半角数字してください" }
    validates :image, presence: { message: "入力してください" }
end
