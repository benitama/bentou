class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :shouhin
      t.integer :tanka
      t.integer :zaikosu
      t.string :image

      t.timestamps null: false
    end
  end
end
